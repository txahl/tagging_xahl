from typing import List, Tuple
import sys
from pathlib import Path
from collections import namedtuple
import regex as re 
import itertools as it
import argparse

EOP = "\u000c"

class Case():
    def __init__(
            self,
            chapter: str,
            title: str,
            content: List[str],
            pages: Tuple[int,int]):
        self.chapter = chapter
        self.title = title
        self.content = content
        self.pages = pages

    def normalize(self) -> str:
        return "\n".join([re.sub("   +","█", line).replace(" ","") for line in self.content])

    def __str__(self):
        return f"{self.chapter}/{self.title} (p.{self.pages})\n{''.join(self.content)}\n=====\n"

    def to_xml_string(self):
        content = "\n".join(self.content)
        normalized = self.normalize()
        return f"""
        <case chapter="{self.chapter.replace(" ","")}" title="{self.title.replace(" ","")}" start="{self.pages[0]}" end="{self.pages[1]}">
            <raw>{content}</raw>
            <normalized>{normalized}</normalized>
        </case>
        """

def print_one(path: Path):
    data = []
    page=1
    chapter=None
    title=None
    text = []
    previous_is_title = False
    start_page = 0
    started = False # on saute l'intro
    for i, line in enumerate(path.read_text().split("\n")):
        line_src = line
        if EOP in line:
            page += 1
            line = line.replace(EOP,"")
        if (not started) or not re.match('.*\p{Han}', line ):
            if (not started) and EOP in line_src and f"刑案匯覽卷" in line_src.replace(" ",""):
                started = True
            continue
        if line.startswith("       "):
            line = line.strip()
            if chapter is None:
                chapter = line
                title = None
            else:
                if len(text) > 0:
                    data.append(Case(chapter, title, text, (start_page, page)))
                    text = []
                if previous_is_title and title is not None:
                    chapter = title
                title = line
            previous_is_title = True
        elif re.match("^ {2,6}\p{Han}", line) and previous_is_title:
            start_page = page
            text = [line.strip()]
            previous_is_title = False
        else:
            text.append(line.strip())
            previous_is_title = False
    
    if len(text) > 0:
        data.append(Case(chapter, title, text, (start_page, page)))
    print("<xahl>")
    
    for chapter, items in it.groupby(data, key=lambda x:x.chapter):
        print(f"<chapter name='{chapter.replace(' ','')}'>")
        for cas in items:
            print(cas.to_xml_string())
        print("</chapter>")
    print("</xahl>")

def main():
    parser = argparse.ArgumentParser()
    parser.add_argument("file", help="text file to convert to xml")
    args = parser.parse_args()
    print_one(Path(args.file))

if __name__ == "__main__":
    main()






















