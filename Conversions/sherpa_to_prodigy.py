from typing import Set, Optional
import json

import sys
import argparse





def convert(path, ann_types: Optional[Set[str]]=None ):
    def filter(ann_label: str) -> bool:
        return ann_types is None or ann_label in ann_types
    with open(path) as f:
        data = json.load(f)
    for doc in data:
        offset = doc['start']
        tokens = [{'text':tok.replace('\n', '\\n'), 'start':i, 'end':i+1, 'id':i, 'ws': False} for i, tok in enumerate(doc['text'])]
        if 'annotations' in doc:
            full_auto = not any(ann['creationMode'] != 'automatic' for ann in doc['annotations'] )
            spans = [
                {
                    'label': ann['labelName'],
                    'start': ann['start'] - offset,
                    'end': ann['end'] - offset,
                    'token_start': ann['start'] - offset,
                    'token_end': ann['end'] -1 - offset
                 }
                for ann in doc['annotations'] if filter(ann['labelName'])
            ]
            session = doc['annotations'][-1]['createdBy']
        else:
            full_auto = True
            spans = []
            session = ""
        prodigy_doc = {
            'meta': doc['metadata'],
            'text': doc['text'],
            'spans': spans,
            "answer": "accept",
            '_session_id': session
        }
        if not full_auto:
            yield prodigy_doc


def main():
    # ann_types = {'secondary_cong', 'leading_case_cheng_an', 'criminal', 'pronouncedjudgement', 'judgement_pan', 'other_party', 'all_other_laws'} 
    ann_types = {'secondary_cong', 'criminal', 'other_party'} 
    parser = argparse.ArgumentParser()
    parser.add_argument("input")
    parser.add_argument('--all', default=False, action="store_true")
    args = parser.parse_args()
    if args.all:
        ann_types = None
    result = convert(args.input, ann_types)
    for line in result:
        print(json.dumps(line))


if __name__ == '__main__':
    main()
