N=1
for f in $(ls ../Data/*.pdf)
do
  NAME=$(basename $f .pdf)
  pdftotext -layout $f - | opencc -c s2t.json > "$NAME.opencc.txt"
  # pdftk $f dump_data_utf8 > "$NAME.toc.txt"
  mv "$NAME.opencc.txt" "../Data/$N.txt"
  N=$(expr $N + 1)
done

# pour la toc :
# pdftk /tmp/a.pdf dump_data_utf8
