let $data := (for $case in collection("test_distam")//case
  return map  { 'metadata': map { 'chapter': data($case/@chapter), 
                  'title': data($case/@title) },
                 'text': $case/normalized/text()
                 
            }
               
)
return json:serialize(array {$data})

