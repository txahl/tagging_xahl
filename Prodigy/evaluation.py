import json
from pathlib import Path

import pandas as pd

def main():
    data={}
    for expe in Path("../Models/").iterdir():
        if expe.is_dir():
            with open(expe / "model-best" / "meta.json") as json_file:
                data[expe.name] = json.load(json_file)
    scores = {}
    scores['global'] = {}
    types = []
    for k,v in data.items():
        scores['global'][k] = v['performance']['ents_f']
        types.extend(v['performance']['ents_per_type'].keys())
    for t in set(types):
        scores[t] = {}
        for k,v in data.items():
            scores[t][k] = v['performance']['ents_per_type'][t]['f'] 
    df = pd.DataFrame(scores)
    df.to_csv("../Results/scores.csv")
    return scores

if __name__ == "__main__":
    main()

