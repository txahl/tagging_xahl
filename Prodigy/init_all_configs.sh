#!/usr/bin/bash

for BASE in $(ls base_config*.cfg)
do 
  TARGET=$(echo $BASE | sed "s/base_//")
  python -m spacy init fill-config "$BASE" "$TARGET"
done
