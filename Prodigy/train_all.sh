#!/usr/bin/bash

DBNAME=$1 # distam_train,eval:distam_eval

for CONFIG in $(ls config_*.cfg)
do
  NAME=$(basename $CONFIG .cfg | sed -s 's/config_//')
  prodigy train --ner "$DBNAME" --lang zh --gpu-id 0 --label-stats -c "$CONFIG" "../Models/$NAME" > "../Models/$NAME.log"
done



