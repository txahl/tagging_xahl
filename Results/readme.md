# chinese-bert
                            P       R       F
official                84.38   95.29   89.50
criminal                65.05   75.81   70.02
secondary_cong          61.75   61.47   61.61
all_other_laws          39.60   37.38   38.46
judgement_pan           62.50   47.87   54.22
pronouncedjudgement     53.49   46.94   50.00
date                    91.89   95.77   93.79
other_party             57.67   59.33   58.49
leading_case_cheng_an   11.11   16.67   13.33
location                50.00   62.50   55.56
province                 0.00    0.00    0.00
