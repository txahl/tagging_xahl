Ce dépôt contient le code et les données créé à l'occasion d'une _résidence DISTAM_ autour d'un projet sur l’identification et l’annotation automatique d’entités au sein de recueils de jugements compilés sous les Qing (1644-1911).

Le projet est décrit dans le billet hypothèse que vous pouvez trovuer en suivant [ce lien](https://distam.hypotheses.org/12692)

L'objectif des codes et des données disponibles ici était de tester l'entraînement de différents modèles disponibles pour le chinois classique sur une tâche de reconnaissance d'entités nommées pour ce domaine juridique particulier.

Le dépôt est organisé de la façon suivante:

- le dossier 'Conversions' contient les scripts de conversion des documents depuis les PDF vers les outils d'annotation
- le dossier 'Data' contient les données (à différentes étapes de la chaîne de traitement)
- le dossier 'Models' contient les logs d'entraînements de différents modèles (pour de multiples _runs_ à comparer)
- le dossier 'Notebooks' contient des extrait de code illustrant l'utilisation des modèles entraînés
- le dossier 'Prodigy' contient les fichiers de configuration pour lancer les expériences avec l'outil [Prodigy](https://prodi.gy)
- le dossier 'W2V' contient des configuration supplémentaires d'expériences utilisant des plongements "statiques" (skipgram) entraînés sur des données du domaine pour remplacer de plus grans modèles de langues plus généralistes.


Les différents types d'entités annotées sont les suivants :

1. Function 職: Name of the office/position who handled the case (直隸司, 廣東撫, 戶部…)
2. Province 省: Name of province(s) that appear in a case
3. all other laws: All laws quoted in a case. Do not annotate as "all other laws" the segment in which the legal provision used to propose the sentence is found (this segment will be annotated as "judgment"; see next entry) [the purpose of the "all other laws" label is to provide a list of all the legal provisions quoted in a specific case]
4. Judgement 判: All laws quoted in a case and used as final legal basis for a sentence, and must include the punishment. As a general rule, for each case only one "judgment" label can be provided.
5. Criminal 首: name of the principal criminal in a case
6. Secondary 從: names of all accomplices mentioned in a case
7. other party: names of all other persons mentioned in a case, with the exception of the criminal 首 and secondaries 從
8. Date: Date of the document (found at the end of the case)
9. Leading case 成案: all information mentioned in a leading case, such as date, province, name of criminal(s) and/or secondary(ies), criminal facts, type of crime, law quoted, and sentence (in general a leading case appears in between the Chinese characters 查 and 案)
10. Pronounced punishment: the actual punishment derived from the final sentence (including the eventual mitigation of the legal punishment or the modification of the punishment pronounced in a previous judgement) [this label marks the final sentence actually pronounced in a case, which may include either the legal punishment or the mitigated punishment]
11. Location: all place names mentioned in a case, with the exception of the province