Les différents types d'entités annotées dans ce projet sont les suivants :

1. Function 職: Name of the office/position who handled the case (直隸司, 廣東撫, 戶部…)
2. Province 省: Name of province(s) that appear in a case
3. all other laws: All laws quoted in a case. Do not annotate as "all other laws" the segment in which the legal provision used to propose the sentence is found (this segment will be annotated as "judgment"; see next entry) [the purpose of the "all other laws" label is to provide a list of all the legal provisions quoted in a specific case]
4. Judgement 判: All laws quoted in a case and used as final legal basis for a sentence, and must include the punishment. As a general rule, for each case only one "judgment" label can be provided.
5. Criminal 首: name of the principal criminal in a case
6. Secondary 從: names of all accomplices mentioned in a case
7. other party: names of all other persons mentioned in a case, with the exception of the criminal 首 and secondaries 從
8. Date: Date of the document (found at the end of the case)
9. Leading case 成案: all information mentioned in a leading case, such as date, province, name of criminal(s) and/or secondary(ies), criminal facts, type of crime, law quoted, and sentence (in general a leading case appears in between the Chinese characters 查 and 案)
10. Pronounced punishment: the actual punishment derived from the final sentence (including the eventual mitigation of the legal punishment or the modification of the punishment pronounced in a previous judgement) [this label marks the final sentence actually pronounced in a case, which may include either the legal punishment or the mitigated punishment]
11. Location: all place names mentioned in a case, with the exception of the province